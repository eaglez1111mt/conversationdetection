# Eagle Zhao
# dapeng.zhao@milwaukeetool.com
# Sep 28 2021


import os
from glob import glob
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
import scipy.io.wavfile
import csv
import moviepy.editor as mp
from scipy.fftpack import fft


def get_all_file(dir, ext):
    File = [file
             for path, subdir, files in os.walk(dir)
                 for file in glob(os.path.join(path, ext))]
    return File




""" Setting """
dir = r"X:\AdvEng\Concept Team\Team Member Folders\Wekwert\Research Videos for Eagle"
clip_length_sec = 5 # sec
sig_ratio_threshold = 0.50





""" Find all .MP4 under a directory """
File = get_all_file(dir, '*.MP4')
n_file = np.zeros(len(File))




""" Process .MP4 one by one """
for i, file in enumerate(File[:]):
    print(i, file)
    file_wav = file[:-3]+"wav"
    if not os.path.exists(file_wav):
        mp.VideoFileClip(file).audio.write_audiofile(file_wav)
    fs, sig = scipy.io.wavfile.read(file_wav)
    sig_len, clip_len = len(sig), clip_length_sec*fs
    Data = []
    for sample_i in range(0,sig_len,clip_len):

        clip = sig[sample_i:min(sample_i+clip_len, sig_len)][:,0] # only use left channel

        B, A = scipy.signal.butter(7, (300/(fs/2), 3400/(fs/2)), 'bandpass', output='ba')
        clip_filt = scipy.signal.filtfilt(B, A, clip)
        # scipy.io.wavfile.write(file_wav[:-3]+str(int(sample_i/fs))+"sec.wav", fs, clip_filt)

        frequency = np.linspace (0, fs/2, int(clip_len/2))
        y = 2/clip_len * np.abs (fft(clip)[0:int(clip_len/2)])
        y_filt = 2/clip_len * np.abs (fft(clip_filt)[0:int(clip_len/2)])

        n_sec = sample_i/fs
        sig_weight, all_weight, sig_ratio = np.sum(y_filt), np.sum(y), np.sum(y_filt)/np.sum(y)
        # print("!!!!" if sig_ratio>0.50 else "____", n_sec//60,'min', n_sec%60,"sec - ", sig_weight, all_weight, sig_ratio)
        Data.append(["%d:%d"%(n_sec//60,n_sec%60),sig_ratio,sig_ratio>sig_ratio_threshold])

        # plt.plot(frequency[:17500], y[:17500])
        # plt.plot(frequency, y, '.')
        # plt.plot(frequency, y_filt, '.')
        # plt.title('Frequency domain Signal')
        # plt.xlabel('Frequency in Hz')
        # plt.ylabel('Amplitude')
        # plt.savefig("%s%dmin%dsec.png" % (file[:-3],n_sec//60,n_sec%60))
        # # plt.show()
        # plt.clf()


    ''' Write CSV '''
    file_csv = file[:-3]+"csv"
    with open(file_csv, 'w', newline='', encoding='utf-8') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(["Time", "Signal_ratio", "Label"])
        for d in Data:
            csvwriter.writerow(d)
